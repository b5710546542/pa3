/**
 * Unit of weight.
 * @author Jidapar Jettananurak.
 *
 */
public enum Weight implements Unit{

	GRAM("Gram" , 1.0),
	CENTIGRAM("Centigram" , 0.01 ),
	KILOGRAM("Kilogram" , 1000.0 ),
	POUND("Pound" , 453.59237 ),
	SALUENG("Salueng" , 3.75 ),
	OUNCE("Ounce" , 28.349523125);
	
	/** name of this unit */
	public final String name;
	/** multiplier to convert this unit to meters */
	public final double value;
	
	/** Constructor for members of the enum */
	private Weight(String name , double value){
		this.value = value;
		this.name = name;
	}
	
	/**Convert unit	
	 * @return number of unit 
	 */
	public double convertTo( double amt , Unit unit ){
		return (this.value*amt)/unit.getValue();
	}
	
	/** public properties of the enum members
	  *@return value;
	  */
	public double getValue(){
		return value;
	}
	
	/**
	 * Tell name of unit.
	 *@return name 
	 */
	public String toString(){
		return this.name;
	}
	
}
