/**
 * UnitType contain several unit.
 * @author Jidapar Jettananurak.
 *
 */
public enum UnitType {

	LENGTH( Length.values() ),
	AREA( Area.values() ),
	WEIGHT( Weight.values() ),
	VOLUME( Volume.values() );
	
	private Unit[] unit;
	
	/**
	 * Constructor
	 * @param unit
	 */
	private UnitType( Unit[] unit ){
		this.unit = unit;
	}
	
	/**
	 * Get unit from each UnitType enum. 
	 * @return unit of enum unit
	 */
	public Unit[] getUnit(){
		return unit;
	}
	
}