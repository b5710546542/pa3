/**
 * UnitConverter can connect between other unit with ConverterUI. 
 * @author Jidapar Jettananurak
 *
 */
public class UnitConverter {

	/**
	 * Convert value from unit to other unit that you want.
	 * @param amount is a number of value.
	 * @param fromUnit is beginning unit.
	 * @param toUnit is unit that you want to convert a value.
	 * @return finish convert value.
	 */
	public double convert(double amount , Unit fromUnit , Unit toUnit){
		return fromUnit.convertTo(amount, toUnit);	
	}
	
	/**
	 * Get unit of uniType.
	 * @param utype is a unit that you want.
	 * @return enum unit 
	 */
	public Unit[] getUnits( UnitType utype ){

		Unit unit[] = UnitType.valueOf(utype.name()).getUnit();
		return unit;
	}
	
}


