import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * ConverterUI will show information from convert.
 * @author Jidapar Jettananurak
 *
 */
public class ConverterUI extends JFrame
{

	/**
	 * this is attribute
	 */
	private JButton convertButton;
	private JButton clearButton;
	private JLabel label;
	private JTextField inputField1;
	private JTextField inputField2;
	private JComboBox<Unit> unitComboBox1;
	private JComboBox<Unit> unitComboBox2;
	private JMenuBar menubar;
	private JMenu unitMenu;
	private JMenuItem menuLength;
	private JMenuItem menuArea;
	private JMenuItem menuWeight;
	private JMenuItem menuVolume;
	private JMenuItem menuExit;
	
	private UnitConverter unitconverter;
	private String stringField1 = "";
	private String stringField2 = "";
	private Unit unitBox1 = null;
	private Unit unitBox2 = null;
	private Unit units[];
	
	/**
	 * Main method is run the UI.
	 * @param args
	 */
	public static void main(String[]args){
		UnitConverter uc = new UnitConverter();
		ConverterUI cu = new ConverterUI(uc);
	}

	/**
	 * Constructer create UI.
	 * @param uc unitconverter.
	 */
	public ConverterUI( UnitConverter uc ) {
		this.unitconverter = uc;
		this.setTitle("Unit Converter");
		this.setVisible(true);
		this.setDefaultCloseOperation( EXIT_ON_CLOSE );
		initComponents( );
	}

	/**
	 * initialize components in the window
	 */
	private void initComponents() {

		menubar = new JMenuBar();
		unitMenu = new JMenu("Unit Type");

		menuLength = new JMenuItem("Length");
		menuArea = new JMenuItem("Area");
		menuWeight = new JMenuItem("Weight");
		menuVolume = new JMenuItem("Volume");
		menuExit = new JMenuItem("Exit");

		unitMenu.add(menuLength);
		unitMenu.add(menuArea);
		unitMenu.add(menuWeight);
		unitMenu.add(menuVolume);
		unitMenu.addSeparator();
		unitMenu.add(menuExit);

		setJMenuBar(menubar);
		menubar.add(unitMenu);
		super.setJMenuBar(menubar);

		Container contents = this.getContentPane();
		LayoutManager layout = new GridLayout( 1,5 );
		contents.setLayout( layout );

		inputField1 = new JTextField(10);
		inputField2 = new JTextField(10);

		convertButton = new JButton("Convert");
		clearButton = new JButton("Clear");

		unitComboBox1 = new JComboBox<Unit>( unitconverter.getUnits(UnitType.LENGTH) );
		unitComboBox2 = new JComboBox<Unit>( unitconverter.getUnits(UnitType.LENGTH) );

		label = new JLabel("=");
		label.setHorizontalAlignment(JLabel.CENTER);
		
		contents.add(inputField1);
		contents.add(unitComboBox1);
		
		contents.add(label);
		contents.add(inputField2);
		contents.add(unitComboBox2);
		contents.add(convertButton);
		contents.add(clearButton);

		ActionListener listenerButton = new ConvertButtonListener( );
		convertButton.addActionListener( listenerButton );
		inputField1.addActionListener(listenerButton );

		ActionListener listenerClear = new ClearButtonListener( );
		clearButton.addActionListener( listenerClear );

		ActionListener exitAction = new ExitAction();
		menuExit.addActionListener( exitAction );
		
		ActionListener listenerMenuLength = new SelectJMenuLength();
		menuLength.addActionListener( listenerMenuLength );
		
		ActionListener listenerMenuWeight = new SelectJMenuWeight();
		menuWeight.addActionListener( listenerMenuWeight );
		
		ActionListener listenerMenuArea = new SelectJMenuArea();
		menuArea.addActionListener( listenerMenuArea );
		
		ActionListener listenerMenuVolume = new SelectJMenuVolume();
		menuVolume.addActionListener( listenerMenuVolume );
		
		this.pack(); 
	}


	/**
	 * AcctionListenner for convertButton.
	 * @author Jidapar Jettananurak
	 *
	 */
	class ConvertButtonListener implements ActionListener {

		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			String s1 = inputField1.getText().trim();
			String s2 = inputField2.getText().trim();
			Unit unit1 = (Unit) unitComboBox1.getSelectedItem( );
			Unit unit2 = (Unit) unitComboBox2.getSelectedItem( );
			double result = 0;

			if(s1.length() == 0 && s2.length() == 0){
				JOptionPane op = new JOptionPane();
				op.showMessageDialog(inputField1, "Don't have a number!");
				return;
			}
			else if ( s1.length() != 0 && (!s1.equals(stringField1) || !unitBox1.equals(unit1) ) ) {
				try{
					double value = Double.valueOf( s1 );
					stringField1 = s1;
					unitBox1 = unit1;
					unitBox2 = unit2;
					result = unitconverter.convert( value, unit1 , unit2 );
					inputField2.setText( String.format("%5g", result ) );
					stringField2 = inputField2.getText();
					
				}catch( NumberFormatException nfe ) {
					JOptionPane op = new JOptionPane();
					op.showMessageDialog(inputField1, "Not a number!");
					return;
				}	
			}
			else{
				try{
					double value = Double.valueOf( s2 );
					stringField2 = s2;
					unitBox1 = unit1;
					unitBox2 = unit2;
					result = unitconverter.convert( value, unit2 , unit1 );
					inputField1.setText( String.format("%5g", result ) );
					stringField1 = inputField1.getText();
					
				}catch( NumberFormatException nfe ) {
					JOptionPane op = new JOptionPane();
					op.showMessageDialog(inputField1, "Not a number!");
					return;
				}	
			}
			
		}
	}


	/**
	 * AcctionListenner for clear data.
	 * @author Jidapar Jettananurak
	 *
	 */
	class ClearButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			inputField1.setText(null);
			inputField2.setText(null);
		}
	}


	/**
	 * AcctionListenner for exit from program.
	 * @author Jidapar Jettananurak
	 *
	 */
	class ExitAction implements ActionListener {
	
		public void actionPerformed(ActionEvent evt) {
			System.exit(0);
		}
	}

	/**
	 * AcctionListenner for change from other unit to length unit.
	 * @author Jidapar Jettananurak
	 *
	 */
	class SelectJMenuLength implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			units = unitconverter.getUnits(UnitType.LENGTH);
			unitComboBox1.removeAllItems();
			unitComboBox2.removeAllItems();
			for( Unit u : units ){ unitComboBox1.addItem( u );}
			for( Unit u : units ){ unitComboBox2.addItem( u );}
			unitBox1 = (Unit) unitComboBox1.getSelectedItem( );
			unitBox2 = (Unit) unitComboBox2.getSelectedItem( );
		}
	}
	
	/**
	 * AcctionListenner for change from other unit to weight unit.
	 * @author Jidapar Jettananurak
	 *
	 */
	class SelectJMenuWeight implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			units = unitconverter.getUnits(UnitType.WEIGHT);
			unitComboBox1.removeAllItems();
			unitComboBox2.removeAllItems();
			for( Unit u : units ){ unitComboBox1.addItem( u );}
			for( Unit u : units ){ unitComboBox2.addItem( u );}
			unitBox1 = (Unit) unitComboBox1.getSelectedItem( );
			unitBox2 = (Unit) unitComboBox2.getSelectedItem( );
		}
	}
	
	/**
	 * AcctionListenner for change from other unit to area unit.
	 * @author Jidapar Jettananurak
	 *
	 */
	class SelectJMenuArea implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			units = unitconverter.getUnits(UnitType.AREA);
			unitComboBox1.removeAllItems();
			unitComboBox2.removeAllItems();
			for( Unit u : units ){ unitComboBox1.addItem( u );}
			for( Unit u : units ){ unitComboBox2.addItem( u );}
			unitBox1 = (Unit) unitComboBox1.getSelectedItem( );
			unitBox2 = (Unit) unitComboBox2.getSelectedItem( );
		}
	}
	
	/**
	 * AcctionListenner for change from other unit to volume unit.
	 * @author Jidapar Jettananurak
	 *
	 */
	class SelectJMenuVolume implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			units = unitconverter.getUnits(UnitType.VOLUME);
			unitComboBox1.removeAllItems();
			unitComboBox2.removeAllItems();
			for( Unit u : units ){ unitComboBox1.addItem( u );}
			for( Unit u : units ){ unitComboBox2.addItem( u );}
			unitBox1 = (Unit) unitComboBox1.getSelectedItem( );
			unitBox2 = (Unit) unitComboBox2.getSelectedItem( );
		}
	}
	
}