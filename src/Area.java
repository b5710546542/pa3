/**
 * Unit of area.
 * @author Jidapar Jettananurak.
 *
 */
public enum Area implements Unit {
	
	SQUAREMETER("Square Meter" , 1.0),
	SQUARECENTIMETER("Square Centimeter" , 0.0001),
	SQUAREKILOMETER("Square Kilometer" , 1000000.0),
	SQUAREMILE("Square Mile" , 2589988.1103),
	SQUAREFOOT("Square Foot" , 0.09290304),
	SQUAREWA("Square Wa" , 4.0);

	/** name of this unit */
	public final String name;
	/** multiplier to convert this unit to meters */
	public final double value;
	
	/** Constructor for members of the enum */
	private Area(String name , double value){
		this.value = value;
		this.name = name;
	}
	
	/**Convert unit	
	 * @return number of unit 
	 */
	public double convertTo(double amt, Unit unit) {
		return (this.value*amt)/unit.getValue();
	}

	/** public properties of the enum members
	  *@return value;
	  */
	public double getValue() {
		return value;
	}
	
	/**
	 * Tell name of unit.
	 *@return name 
	 */
	public String toString(){
		return this.name;
	}
	
}
