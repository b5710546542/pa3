/**
 * Unit of length.
 * @author Jidapar Jettananurak.
 *
 */
public enum Length implements Unit {

	METER("Meter" , 1.0),
	CENTIMETER("Centimeter" , 0.01),
	KILOMETER("Kilometer" , 1000.0),
	MILE("Mile" , 1609.344),
	FOOT("Foot" , 0.3048),
	WA("Wa" , 2.0);
	
	/** name of this unit */
	public final String name;
	/** multiplier to convert this unit to meters */
	public final double value;
	
	/** Constructor for members of the enum */
	private Length(String name , double value){
		this.value = value;
		this.name = name;
	}
	
	/**Convert unit	
	 * @return number of unit 
	 */
	public double convertTo( double amt , Unit unit ){
		return (this.value*amt)/unit.getValue();
	}
	
	/** public properties of the enum members
	  *@return value;
	  */
	public double getValue(){
		return value;
	}
	
	/**
	 * Tell name of unit.
	 *@return name 
	 */
	public String toString(){
		return this.name;
	}
	
}
