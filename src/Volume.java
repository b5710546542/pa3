/**
 * Unit of volume.
 * @author Jidapar Jettananurak.
 *
 */
public enum Volume implements Unit {
	
	CUBICMETER("Cubic meter" , 1.0),
	GALLON("Gallon" , 0.00454609 ),
	BARREL("Barrel" , 0.158987294928 ),
	THANG("Thang" , 0.02 ),
	LITER("Liter" , 0.001 ),
	CUBICCENTIMETER("Cubic centimeter" , 0.000001 );
	
	/** name of this unit */
	public final String name;
	/** multiplier to convert this unit to meters */
	public final double value;
	
	/** Constructor for members of the enum */
	private Volume(String name , double value){
		this.value = value;
		this.name = name;
	}
	
	/**Convert unit	
	 * @return number of unit 
	 */
	public double convertTo( double amt , Unit unit ){
		return (this.value*amt)/unit.getValue();
	}
	
	/** public properties of the enum members
	  *@return value;
	  */
	public double getValue(){
		return value;
	}
	
	/**
	 * Tell name of unit.
	 *@return name 
	 */
	public String toString(){
		return this.name;
	}
	
}
